package com.example.sampleapp.Class;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.sampleapp.R;

import java.util.ArrayList;
import java.util.HashMap;

public class ListAdapterItem extends RecyclerView.Adapter<ListAdapterItem.ViewHolder> {
    private String url_imgg;

    private Context context;
    private ArrayList<HashMap<String, String>> list_data;

    private static OnItemClickListener onItemClickListener;

    public ListAdapterItem(Context mainActivity, ArrayList<HashMap<String, String>> list_data) {
        this.context = mainActivity;
        this.list_data = list_data;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        ListAdapterItem.onItemClickListener = onItemClickListener;

    }

    public static interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String id     = list_data.get(position).get("id");
        String isi      = list_data.get(position).get("invoice_no");

        holder.titlemessage.setText(isi);
        holder.titlemessage.setTypeface(null, Typeface.BOLD);

    }

    @Override
    public int getItemCount() {
        int a ;

        if(list_data != null && !list_data.isEmpty()) {

            a = list_data.size();
        }
        else {

            a = 0;

        }

        return a;
        //return list_data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView titlemessage, dateMessage, messagetrun, tglDisposisi;

        ViewHolder(View itemView) {
            super(itemView);

            titlemessage= itemView.findViewById(R.id.tvItem);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position  = ViewHolder.super.getAdapterPosition();
                    onItemClickListener.onItemClick(view, position);
                }
            });
        }
    }
}
