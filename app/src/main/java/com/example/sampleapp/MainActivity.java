package com.example.sampleapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sampleapp.Class.CustomToast;
import com.example.sampleapp.Class.ListAdapterItem;
import com.example.sampleapp.Class.RequestHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private RecyclerView listView;
    private ListAdapterItem adapter;
    ArrayList<HashMap<String, String>> list_data;
    LayoutInflater inflater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.recyclerView);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(RecyclerView.VERTICAL);
        listView.setLayoutManager(llm);

        getList();
    }

    private void getList(){
        class GetListPembelian extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(MainActivity.this,"Mengambil...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                try {
                    Log.d("data", s);
                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    if (jsonArray.length() == 0){
                        Toast.makeText(MainActivity.this, "Tidak Ditemukan!", Toast.LENGTH_SHORT).show();
                    }else {
                        showListPembelian(s);
                    }
                }catch (JSONException a){
                    a.printStackTrace();
                    Toast.makeText(MainActivity.this, "No Internet Connection!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequest("http://api-android.blackboxcoffe.com/api/transaction/receipt");
                return s;
            }
        }
        GetListPembelian ge = new GetListPembelian();
        ge.execute();
    }

    private void showListPembelian(String s) {

        list_data = new ArrayList<HashMap<String, String>>();
        Log.d("response ", s);
        try {
            JSONObject jsonObject = new JSONObject(s);
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            if (jsonArray.length() != 0) {
                for (int a = 0; a < jsonArray.length(); a++) {
                    JSONObject json = jsonArray.getJSONObject(a);

                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("id", json.getString("id"));
                    map.put("invoice_no", json.getString("invoice_no"));
                    map.put("items", json.getString("items"));
                    map.put("customer", json.getString("customer"));
                    map.put("payment", json.getString("payment"));
                    map.put("total", json.getString("total"));
                    map.put("notes", json.getString("notes"));
                    map.put("user_id", json.getString("user_id"));
                    map.put("created_at", json.getString("created_at"));
                    list_data.add(map);
                    adapter = new ListAdapterItem(MainActivity.this, list_data);
                    ((ListAdapterItem) adapter).setOnItemClickListener(new ListAdapterItem.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            HashMap<String, String> map = (HashMap) list_data.get(position);
                            String empId = map.get("id").toString();
                            detailInvoice(map);
//                            Toast.makeText(MainActivity.this, empId, Toast.LENGTH_SHORT).show();
//                            Intent intent = new Intent(MainActivity.this, DetailInboxKepala.class);
//                            HashMap<String, String> map = (HashMap) list_data.get(position);
//                            String empId = map.get(Config.TAG_ID_SURAT_MSK).toString();
//                            intent.putExtra(Config.EMP_ID, empId);
//                            startActivity(intent);
                        }
                    });
                    listView.setAdapter(adapter);
                }
            } else {
                Toast.makeText(MainActivity.this, "Pembelian Kosong!", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e){
            Toast.makeText(MainActivity.this, "No Internet Connection!", Toast.LENGTH_SHORT).show();
        }
    }

    private void detailInvoice(HashMap<String, String> map){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.modal_invoice, null);
        TextView detailAInvoice = dialogView.findViewById(R.id.detailInvoice);
        String total = map.get("total").toString();
        String a = "";
        try {
            JSONObject obj = new JSONObject(map);
            String notes = obj.getString("items");
            JSONArray arr = new JSONArray(notes); // notice that `"posts": [...]`
            for (int i = 0; i < arr.length(); i++){
                String name = arr.getJSONObject(i).getString("name");
                a = a.concat(name + "<br/>");
            }
        }catch (JSONException e){
            Log.e("name ", String.valueOf(e));
        }
        detailAInvoice.setText(Html.fromHtml("<h4>" + a + "</h4><br/><p>--------------------<br/>" + total + "<br/>--------------------</p>"));
        alertDialogBuilder
                .setView(dialogView)
                .setCancelable(false)
                .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        // membuat alert dialog dari builder
        AlertDialog alertDialog = alertDialogBuilder.create();

        // menampilkan alert dialog
        alertDialog.show();
    }
}